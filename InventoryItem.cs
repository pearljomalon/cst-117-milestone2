namespace InventoryManagementProgram
{
    class InventoryItem
    {
        private string type;
        private string name;
        private string brand;
        private int quantity;
        private decimal price;

        public InventoryItem()
        {

        }

        public InventoryItem(string type, string name, string brand, int quantity, decimal price)
        {
            this.type = type;
            this.name = name;
            this.brand = brand;
            this.quantity = quantity;
            this.price = price;
        }

        public string Type
        {
            get => type;
            set => type = value;
        }

        public string Name
        {
            get => name;
            set => name = value;
        }

        public string Brand
        {
            get => brand;
            set => brand = value;
        }

        public int Quantity
        {
            get => quantity;
            set => quantity = value;
        }

        public decimal Price
        {
            get => price;
            set => price = value;
        }
    }
}
