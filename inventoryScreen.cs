using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace InventoryManagementProgram
{
    public partial class inventoryScreen : Form
    {
        public inventoryScreen()
        {
            InitializeComponent();

            List<InventoryItem> inventoryList = Program.inventoryList;

            int row = 0;

            foreach (InventoryItem inventoryItem in inventoryList)
            {
                inventoryDataGrid.Rows.Insert(row);
                inventoryDataGrid[0, row].Value = inventoryItem.Type;
                inventoryDataGrid[1, row].Value = inventoryItem.Name;
                inventoryDataGrid[2, row].Value = inventoryItem.Brand;
                inventoryDataGrid[3, row].Value = inventoryItem.Quantity;
                inventoryDataGrid[4, row].Value = inventoryItem.Price;

                row++;
            }
        }

        private void homeButton_Click(object sender, EventArgs e)
        {
            homeScreen homeScreen = new homeScreen();

            this.Hide();
            homeScreen.Show();
        }

        private void viewButton_Click(object sender, EventArgs e)
        {
            individualScreen individualScreen = new individualScreen();

            this.Hide();
            individualScreen.Show();
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
