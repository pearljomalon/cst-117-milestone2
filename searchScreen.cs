using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InventoryManagementProgram
{
    public partial class searchScreen : Form
    {
        public searchScreen()
        {
            InitializeComponent();
        }

        private void homeButton_Click(object sender, EventArgs e)
        {
            homeScreen homeScreen = new homeScreen();

            this.Hide();
            homeScreen.Show();
        }

        private void viewButton_Click(object sender, EventArgs e)
        {
            individualScreen individualScreen = new individualScreen();

            this.Hide();
            individualScreen.Show();
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
